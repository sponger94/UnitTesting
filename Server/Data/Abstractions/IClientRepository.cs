﻿using System;
using System.Collections.Generic;
using System.Text;
using Server.Model;

namespace Server.Data.Abstractions
{
    public interface IClientRepository : IRepository<Client>
    {
        Client GetBy(int id);
    }
}
