﻿using System;

namespace Server.Data.Abstractions
{
    public interface IContext : IDisposable
    {
        IClientRepository Clients { get; }
        int SaveChanges();
    }
}
