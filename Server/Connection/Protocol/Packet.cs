﻿namespace Server.Connection.Protocol
{
    public class Packet : IPacket
    {
        public int PacketId { get; set; }
        public string Key { get; set; }
        public byte[] Data { get; set; }
        public SenderPoint Sender { get; set; }
        public PacketType PacketType { get; set; }
    }
}
