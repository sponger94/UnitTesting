﻿namespace Server.Connection.Protocol
{
    public interface IPacket
    {
        int PacketId { get; set; }
        string Key { get; set; }
        byte[] Data { get; set; }
        SenderPoint Sender { get; set; }
        PacketType PacketType { get; set; }
    }

    public enum SenderPoint : byte
    {
        Client,
        Server
    }

    public enum PacketType : byte
    {
        Request,
        Response
    }
}
