﻿using Server.Connection.Protocol;

namespace Server.Connection.Abstractions
{
    public interface IClientAppSession
    {
        bool Authorized { get; set; }
        int ClientId { get; set; }
        string SessionId{ get; set; }
        void Send(IPacket packet);
        void Close();
    }
}
