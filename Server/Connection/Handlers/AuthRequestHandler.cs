﻿using System;
using Server.Connection.Abstractions;
using Server.Connection.Protocol;
using Server.Services;

namespace Server.Connection.Handlers
{
    public sealed class AuthRequestHandler : IClientRequestHandler
    {
        private IClientService _clientService;
        private ISecurityService _securityService;

        public AuthRequestHandler()
        {
            _clientService = Server.Instance.ClientService;
            _securityService = Server.Instance.SecurityService;
        }

        public IPacket HandleRequest(IPacket packet, IClientAppSession session)
        {
            var serializer = Server.Instance.Serializer;
            var authInfo = serializer.Deserialize<AuthInfo>(packet.Data);

            if (!_securityService.Authenticate(authInfo.Username, authInfo.PasswordHash))
            {
                return new Packet { Key = "REG", Sender = SenderPoint.Server, PacketType = PacketType.Request};
            }
            session.Authorized = true;
            return new Packet { PacketType = PacketType.Response};
        }
    }

    public sealed class AuthInfo
    {
        public string Username { get; set; }
        public string PasswordHash { get; set; }
    }

    public sealed class AuthorizationFailedException : Exception
    {
        public AuthorizationFailedException(string message) : base(message)
        {
        }
    }
}
