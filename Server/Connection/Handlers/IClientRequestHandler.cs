﻿using Server.Connection.Abstractions;
using Server.Connection.Protocol;

namespace Server.Connection.Handlers
{
    public interface IClientRequestHandler
    {
        IPacket HandleRequest(IPacket packet, IClientAppSession session);
    }
}
