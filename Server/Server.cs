﻿using Server.Serialization;
using Server.Services;

namespace Server
{
    public class Server
    {
        private static Server _singleton;
        public static Server Instance => _singleton ?? (_singleton = new Server());

        public ISerializer Serializer { get; internal set; }
        public IClientService ClientService { get; internal set; }
        public ISecurityService SecurityService { get; internal set; }

        public Server()
        {
            
        }
    }
}
