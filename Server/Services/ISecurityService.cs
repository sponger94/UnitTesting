﻿namespace Server.Services
{
    public interface ISecurityService
    {
        bool Authenticate(string username, string passwordHash);
    }
}
