﻿using System;
using Server.Model;

namespace Server.Services
{
    public interface IClientService
    {
        Client GetBy(int id);

        Client Create(string username, string passwordHash, DateTime birthDate);
    }
}
