﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Server.Model
{
    public class Client
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string PasswordHash { get; set; }
        public DateTime BirthDate { get; set; }
    }
}
